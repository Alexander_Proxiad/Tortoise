package bg.proxiad.demo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SayHelloTask3
 */
public class SayHelloTask3 extends HttpServlet {
	AddOn addOn;
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.print("You have clicked on this ammount of pages " + addOn.count++ );
		String greet = getServletConfig().getInitParameter("greet");
		String name = request.getParameter("name");
		out.print(greet + " " + name);
		
	}

	

}
