package bg.proxiad.demo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CountAllRequests
 */
public class CountAllRequests extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();

		HttpSession session = request.getSession();

		int sayHelloCount = (Integer) session.getAttribute("sayHelloCount");
		int hitCount = (Integer) session.getAttribute("hitCount");
		int sayHelloServlet = (Integer) session.getAttribute("sayHelloServlet");
		
		int sum = sayHelloCount + hitCount + sayHelloServlet;
		

		out.println("You have clicked on : " + sum);
	}

}
