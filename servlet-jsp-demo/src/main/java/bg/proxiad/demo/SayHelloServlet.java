package bg.proxiad.demo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SayHelloServlet extends HttpServlet {
	AddOn addOn;
	public int sayHelloServlet = 0;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		
		out.print("You have clicked on this ammount of pages " + addOn.count++ );
		
		
		
		sayHelloServlet++;
		resp.getWriter().write("Hello from a servlet");
		
		HttpSession session = req.getSession();
		session.setAttribute("sayHelloServlet", sayHelloServlet);
		
		
		
	}

	
	
}
