package bg.proxiad.demo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class HowManyHits extends HttpServlet {
	private static final long serialVersionUID = 1L;
	AddOn addOn;
   private int hitCount = 0;
	
	public void init(ServletConfig config) throws ServletException {
		hitCount = 0;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		hitCount++;
		PrintWriter out = response.getWriter();
		
		out.println("You have entered this page : " + hitCount + " times");
		
		out.print("You have clicked on this ammount of pages " + addOn.count++ );
		
		HttpSession session = request.getSession();
		session.setAttribute("hitCount", hitCount);
		
		
		
	}

	

}
