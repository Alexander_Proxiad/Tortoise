package spring.exercise;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Question {

	private int id;
	private String name;
	private Map<Answer, User> answerMap;

	public Question() {

	}

	public Question(int id, String name, Map<Answer, User> answerMap) {
		super();
		this.id = id;
		this.name = name;
		this.answerMap = answerMap;
	}

	public void displayInfo() {
		System.out.println("question id:" + id);
		System.out.println("question name:" + name);
		System.out.println("Answers....");
		Set<Entry<Answer, User>> set = answerMap.entrySet();
		Iterator<Entry<Answer, User>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Entry<Answer, User> entry = iterator.next();
			Answer answer = entry.getKey();
			User user = entry.getValue();
			System.out.println("Answer Information:");
			System.out.println(answer);
			System.out.println("Posted By:");
			System.out.println(user);
		}
	}
}
