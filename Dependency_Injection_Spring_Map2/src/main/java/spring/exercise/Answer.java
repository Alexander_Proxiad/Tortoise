package spring.exercise;

import java.util.Date;

public class Answer {

	private int id;
	private String answer;
	private Date dateposted;

	public Answer() {
		
	}

	public Answer(int id, String answer, Date dateposted) {
		super();
		this.id = id;
		this.answer = answer;
		this.dateposted = dateposted;
	}

	@Override
	public String toString() {
		return "Answer [id=" + id + ", answer=" + answer + ", dateposted=" + dateposted + "]";
	}

}
