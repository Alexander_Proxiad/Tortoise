package mockito.javacodegeeks;

public interface AuthenticatorInterface {
	
	public boolean authenticateUser(String username,String password);

}
