package mockito.javacodegeeks;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.Mockito;

public class AuthenticatorApplicationTest {

	@Test
	public void test() {
		AuthenticatorInterface authenticatorMock;
		AuthenticatorApplication authenticator;
		String username = "JavaCodeGeeks";
		String password = "unsafePassowrd";
		authenticatorMock = Mockito.mock(AuthenticatorInterface.class);
		authenticator = new AuthenticatorApplication(authenticatorMock);
		
		when(authenticatorMock.authenticateUser(username, password)).thenReturn(false);
		boolean actual = authenticator.authenticate(username, password);
		assertFalse(actual);
	}

}
