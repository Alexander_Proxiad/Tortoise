package luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import luv2code.hibernate.demo.entity.Course;
import luv2code.hibernate.demo.entity.Instructor;
import luv2code.hibernate.demo.entity.InstructorDetail;
import luv2code.hibernate.demo.entity.Review;
import luv2code.hibernate.demo.entity.Student;

public class CreateCourseAndStudentsDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.addAnnotatedClass(Review.class)
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();
		
		try {
			
			session.beginTransaction();
			
		
			//create a course
			Course tempCourse = new Course("pacman - how to score one million points");
			System.out.println("\nSaving the course...");
			System.out.println();
			session.save(tempCourse);
			System.out.println("Saved the course " + tempCourse);
			
			Student tempStudent1 = new Student("Alex", "Vulchev", "Aspeks@mail.bg");
			Student tempStudent2 = new Student("Svenja", "Ruprecht", "sleeping@de.de");
			
			tempCourse.addStudent(tempStudent1);
			tempCourse.addStudent(tempStudent2);
			
			System.out.println("\nSaving students...");
			
			session.save(tempStudent1);
			session.save(tempStudent2);
			
			System.out.println("Saved students " + tempCourse.getStudents());
			
			session.getTransaction().commit();
			System.out.println("DONE");
			
		}finally {
			session.close();
			factory.close();
		}

	}

}
