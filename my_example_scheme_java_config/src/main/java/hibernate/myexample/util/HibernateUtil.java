package hibernate.myexample.util;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import hibernate.myexample.entity.Person;

public class HibernateUtil {
	
	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			return new Configuration().addProperties(createProperties())
					.addAnnotatedClass(Person.class)
					.buildSessionFactory();
		}catch(Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
		
	}

	private static Properties createProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.connection.driver_class", "com.mysql.cj.jdbc.Driver");
		properties.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/my-example-scheme");
		properties.setProperty("hibernate.connection.username", "root");
		properties.setProperty("hibernate.connection.password", "19941905a");
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		properties.setProperty("hibernate.show_sql", "true");
		properties.setProperty("hibernate.hbm2ddl.auto", "create");
		return properties;
	}
	
	public static void shutdown() {
		getSessionFactory().close();
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	

}
