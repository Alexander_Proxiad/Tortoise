

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class EditServlet
 */
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		//getting the unique ID in order to operate on it
		int userId = Integer.valueOf((String)request.getParameter("userId"));
		HttpSession session = request.getSession();
		//getting the userList attributed that is stored in the session scope
		Map<Integer, User> userList = (Map<Integer, User>)session.getAttribute("userList");
		User user = userList.get(userId);//getting the current user id so i can overwrite it
		request.setAttribute("user", user);//saving the user in the session scope
		request.getRequestDispatcher("edituser.jsp").forward(request, response);//going to the edit page
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession();//establishing session
		
		int userId = Integer.valueOf((String)request.getParameter("userId"));//getting the id of the user i just edited
		//getting all the edited parameters
		String editName = request.getParameter("userName");
		String editPassword = request.getParameter("userPassword");
		String editEmail = request.getParameter("userEmail");
		String editGender = request.getParameter("userGender");
		String editCountry = request.getParameter("userCountry");
		
		Map<Integer,User> userList = (Map<Integer,User>)session.getAttribute("userList");//getting the userList from the session
		//overriding the previous data of the user
		User user = new User();
		user.setId(userId);
		user.setName(editName);
		user.setPassword(editPassword);
		user.setEmail(editEmail);
		user.setGender(editGender);
		user.setCountry(editCountry);
		
		userList.put(userId, user);//adding the edited user to the userList
		session.setAttribute("userList", userList);//saving the list onto the session 
		
		//response.sendRedirect("viewusers.jsp");
		request.getRequestDispatcher("viewusers.jsp").forward(request, response);//viewing the userList page
	}
	

}
