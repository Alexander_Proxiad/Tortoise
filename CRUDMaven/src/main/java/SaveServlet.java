

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SaveServlet
 */
public class SaveServlet extends HttpServlet {
	static int userId = 0;
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");//can write html code in the output
		PrintWriter out = response.getWriter();
		
		userId++;//on every iteration the user Id goes up by one so it can be unique
		
		
		HttpSession session = request.getSession();//establishing session
		
		//getting the parameters from the form
		String userName = request.getParameter("userName");
		String userPassword = request.getParameter("userPassword");
		String userEmail = request.getParameter("userEmail");
		String userGender = request.getParameter("userGender");
		String userCountry = request.getParameter("userCountry");
		//creating a map to store all the users with unique key
		Map<Integer,User> userList = session.getAttribute("userList") != null ? (Map<Integer,User>)session.getAttribute("userList") : new HashMap<Integer,User>();//on first iteration creates a new hashmap
		//creating a new user and setting up all the parameters
		User user = new User();
		user.setId(userId);
		user.setName(userName);
		user.setPassword(userPassword);
		user.setEmail(userEmail);
		user.setGender(userGender);
		user.setCountry(userCountry);
		userList.put(userId,user);//inserting the user into the list
		session.setAttribute("userList", userList);//saving the userList(Map) into the session scope
		
		response.sendRedirect("adduser-success.jsp");//redirecting to the success add page
	}

}
