<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View Users</title>
</head>
<body>
	<table border="1">
		<tr><th>ID</th><th>Name</th><th>Password</th><th>Email</th><th>Gender</th><th>Country</th><th>Edit</th><th>Delete</th></tr>
		<c:forEach items="${userList}" var="entry">
			<tr>
			<td>${entry.key}</td>
			<td>${entry.value.name}</td>
			<td>${entry.value.password}</td>
			<td>${entry.value.email}</td>
			<td>${entry.value.gender}</td>
			<td>${entry.value.country}</td>
			<td><a href="edit?userId=${entry.key}">Edit</a></td>
			<td><a href="delete?userId=${entry.key}">Delete</a></td>
			</tr>		
		</c:forEach>
	</table>
	<br/><a href="adduserform.jsp"></a>
	<br/><a href="">Go to HomePage</a>
</body>
</html>