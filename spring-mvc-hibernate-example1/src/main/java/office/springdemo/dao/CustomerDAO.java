package office.springdemo.dao;

import java.util.List;

import office.springdemo.entity.Customer;

public interface CustomerDAO {
	
	public List<Customer> getCustomers();

}
