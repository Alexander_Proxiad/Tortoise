package office.springdemo.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import office.springdemo.entity.Customer;
@Repository
public class CustomerDAOImple implements CustomerDAO {
	
	//need to inject the session factory
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public List<Customer> getCustomers() {
		//get the current hibernate session
		Session session = sessionFactory.getCurrentSession();
		//create a query
		Query<Customer> theQuery = session.createQuery("from Customer",Customer.class);
		//get the list of customer from the query(execute)
		List<Customer> customers = theQuery.getResultList();	
		//return the list of customer that have been retrieved
		return customers;
	}

}
