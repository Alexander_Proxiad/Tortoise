package spring.java.config;

public class TextEditor {
	
	private SpellChecker spellChecker;

	public TextEditor(SpellChecker spellChecker) {
		super();
		System.out.println("this is TextEditor CONSTRUCTOR");
		this.spellChecker = spellChecker;
	}
	
	public void spellCheck() {
		spellChecker.checkSpelling();
	}

}
