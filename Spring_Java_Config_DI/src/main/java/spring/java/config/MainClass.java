package spring.java.config;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainClass {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		AnnotationConfigApplicationContext aContext = new AnnotationConfigApplicationContext(TextEditorConfig.class);
		TextEditor te = aContext.getBean(TextEditor.class);
		te.spellCheck();

	}

}
