import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {

	public static void main(String[] args) {
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		HelloWorld hw = (HelloWorld) context.getBean("helloWorld");
		//hw.setMessage("AAA");
		hw.printMessage();
		//HelloWorld hw2 = (HelloWorld) context.getBean("helloWorld");
		//hw2.printMessage();
		//((ConfigurableApplicationContext) context).close();
		context.registerShutdownHook();
	}

}
