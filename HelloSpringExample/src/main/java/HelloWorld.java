public class HelloWorld {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void printMessage() {
		System.out.println("Your message: " + message);
	}

	public void init() {
		System.out.println("Bean is going thru the init method");
	}

	public void destroy() {
		System.out.println("Bean is going thru the destroy method");
	}

}
