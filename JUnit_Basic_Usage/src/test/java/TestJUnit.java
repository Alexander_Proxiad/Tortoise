import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TestJUnit {
	
//	@Test
//	public void test() {
//		fail("Not yet implemented");
//	}
	
	String message = "Hello World";
	MessageUtil messageUtil = new MessageUtil(message);
	
	@Test
	public void testPrintMessage() {
//		for the program to fail
//		message = "New Message Fail";
		assertEquals(message, messageUtil.printMessage());
	}

	
}
