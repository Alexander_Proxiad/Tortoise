package com.tutorialspoint;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class Logging {
	
	@Pointcut("execution(* com.tutorialspoint.*.*(..))")
	private void selectAll() {}
	
	@Before("selectAll()")
	public void beforeAdvice(){
	      System.out.println("Going to setup student profile.");
	   }  
}
