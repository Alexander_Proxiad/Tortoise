import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import exercise.repository.HibernatePersonRepositoryImplementation;
import exercise.repository.PersonRepository;
import exercise.service.PersonService;
import exercise.service.PersonServiceImplementation;

@Configuration
public class ApplicationConfig {
	@Bean(name="personService")
	public PersonService getPersonService() {
		PersonServiceImplementation service = new PersonServiceImplementation(getPersonRepository());
		//service.setPersonRepository(getPersonRepository());
		return service;
	}
	
	@Bean(name="personRepository")
	public PersonRepository getPersonRepository() {
		return new HibernatePersonRepositoryImplementation();
	}

}
