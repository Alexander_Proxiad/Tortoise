import exercise.service.PersonService;
import exercise.service.PersonServiceImplementation;

public class Application {

	public static void main(String[] args) {
		
		PersonService service = new PersonServiceImplementation();
		
		System.out.println(service.findAll().get(0).getFirstName());
		System.out.println(service.findAll().get(0).getLastName());

	}

}
