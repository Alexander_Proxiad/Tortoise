package exercise.repository;

import java.util.List;

import exercise.model.Person;

public interface PersonRepository {

	List<Person> findAll();

}