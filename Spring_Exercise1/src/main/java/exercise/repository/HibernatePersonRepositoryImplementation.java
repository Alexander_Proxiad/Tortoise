package exercise.repository;

import java.util.ArrayList;
import java.util.List;

import exercise.model.Person;

public class HibernatePersonRepositoryImplementation implements PersonRepository {

	/* (non-Javadoc)
	 * @see exercise.repository.PersonRepository#findAll()
	 */
	@Override
	public List<Person> findAll() {
		List<Person> persons = new ArrayList<>();

		Person person = new Person();
		person.setFirstName("Alexander");
		person.setLastName("Vulchev");

		persons.add(person);
		return persons;
	}

}
