package exercise.service;

import java.util.List;

import exercise.model.Person;

public interface PersonService {

	List<Person> findAll();

}