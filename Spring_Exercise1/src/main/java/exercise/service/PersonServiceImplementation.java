package exercise.service;

import java.util.List;

import exercise.model.Person;
import exercise.repository.HibernatePersonRepositoryImplementation;
import exercise.repository.PersonRepository;

public class PersonServiceImplementation implements PersonService {
		//upcasting from interface to the implementation
		private PersonRepository personRepository = new HibernatePersonRepositoryImplementation();
		
		/* (non-Javadoc)
		 * @see exercise.service.PersonService#findAll()
		 */
		@Override
		public List<Person> findAll(){
			return personRepository.findAll();
		}
}
