package tutorial.spring;

public class HelloWorld {
	
	private String message;

	public void getMessage() {
		System.out.println("Your message: " + message);
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public void init() {
		System.out.println("Init method");
	}
	
	public void destroy() {
		System.out.println("Destroy method");
	}

}
