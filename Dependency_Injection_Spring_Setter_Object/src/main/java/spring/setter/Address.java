package spring.setter;

public class Address {

	@Override
	public String toString() {
		return "Address [city=" + city + ", country=" + country + ", state=" + state + "]";
	}

	private String city;
	private String country;
	private String state;

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public String getState() {
		return state;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setState(String state) {
		this.state = state;
	}

}
