package spring.setter;

public class Person {

	private int age;
	private String name;
	private Address address;

	public Address getAddress() {
		return address;
	}

	public int getAge() {
		return age;
	}

	public String getName() {
		return name;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void display() {
		System.out.println(age + " " + name);
		System.out.println(address);
	}

}
