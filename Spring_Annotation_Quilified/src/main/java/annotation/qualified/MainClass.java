package annotation.qualified;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		Profile profile = (Profile)context.getBean("profile");
		profile.printAge();
		profile.printName();
	}

}
