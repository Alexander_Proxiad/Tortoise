import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import config.math.AppConfig;
import service.math.layer.MathService;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {AppConfig.class})
public class TestMath {

	@Autowired
	@Qualifier("mathMultiplyImpl")
	MathService ms;

	@Test
	public void test() {
		assertThat(ms.multiplyThreeNumbers(3, 3, 3)).isEqualTo(27);
		assertThat(ms).isInstanceOf(MathService.class);
		assertThat(ms).isNotNull();
		assertThat(ms.multiplyThreeNumbers(3, 3, 3)).isLessThan(50);
		assertThat(ms.multiplyThreeNumbers(3, 3, 3)).isGreaterThan(10);
	}

}
