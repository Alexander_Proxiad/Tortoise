import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import config.math.AppConfig;
import service.math.layer.MathServiceImpl;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {AppConfig.class})
public class TestMathService {
	@Autowired
	@Qualifier("mathServiceImpl")
	MathServiceImpl msi;

	@Test
	public void test() {
		
		assertThat(msi.addTwoNumbers(2, 2)).isEqualTo(4);
		
	}

}