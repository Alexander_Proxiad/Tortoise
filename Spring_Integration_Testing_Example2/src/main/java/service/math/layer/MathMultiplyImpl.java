package service.math.layer;

import org.springframework.stereotype.Service;

@Service("mathMultiplyImpl")
public class MathMultiplyImpl implements MathService{

	@Override
	public double multiplyThreeNumbers(double num1, double num2, double num3) {
		
		return num1*num2*num3;
	}

	@Override
	public double addTwoNumbers(double num1, double num2) {
		// TODO Auto-generated method stub
		return 0;
	}

}
