package service.math.layer;

import org.springframework.stereotype.Service;


public interface MathService {
	
	public double addTwoNumbers(double num1,double num2);
	public double multiplyThreeNumbers(double num1,double num2,double num3);

}
