package service.math.layer;

import org.springframework.stereotype.Service;

@Service("mathServiceImpl")
public class MathServiceImpl implements MathService{

	public double addTwoNumbers(double num1, double num2) {
		return num1 + num2;
	}

	@Override
	public double multiplyThreeNumbers(double num1, double num2, double num3) {
		// TODO Auto-generated method stub
		return 0;
	}

}
