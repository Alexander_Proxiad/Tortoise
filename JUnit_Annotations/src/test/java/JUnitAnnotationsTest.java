import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class JUnitAnnotationsTest {

	//execute before Class
	@BeforeClass
	public static void beforeClass() {
		System.out.println("<<<We are in the beforeClass()>>>");
	}
	
	//execute after class
	@AfterClass
	public static void afterClass() {
		System.out.println("<<<We are in the afterClass()>>>");
	}
	
	//execute before @Test
	@Before
	public void before() {
		System.out.println("<<<Using the @Before anotation>>>");
	}
	
	//execute after @Test
	@After
	public void after() {
		System.out.println("<<<Using the @After anotation>>>");
	}
	
	//test case
	@Test
	public void testCase1() {
		System.out.println("<<<IN THE TEST CASE 1>>>");
	}
	
	@Test
	public void testCase2() {
		System.out.println("<<<IN THE TEST CASE 2>>>");
	}
	
	//test case ignore and will not execute
	@Ignore
	public void ignore() {
		System.out.println("<<<Using the @Ignore anotation,but this will not execute>>>");
	}
}
