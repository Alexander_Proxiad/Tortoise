<%@page import="java.util.*,java.text.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Resoponse Example in JSP</title>
</head>
<body>
	<% response.setIntHeader("Refresh", 3); 
	Date currentDate = new Date();
	SimpleDateFormat ft = new SimpleDateFormat("EEE,d MMM yyyy HH:mm:ss");
	String currentDateTime =ft.format(currentDate);
			%>
	<p>Page last refreshed at: <%= currentDateTime %> </p>
</body>
</html>