import static org.junit.Assert.assertEquals;

import org.junit.Test;

import business.layer.EmpBusinessLogic;
import junit.employee.EmployeeDetails;

public class TestEmployeeDetails {
	
	EmpBusinessLogic empBusinessLogic = new EmpBusinessLogic();
	EmployeeDetails employee = new EmployeeDetails();
	
	//test to check appraisal
	@Test
	public void testCalculateAppraisal() {
		employee.setName("Alex");
		employee.setAge(24);
		employee.setMonthlySalary(8000);
		
		double appraisal = empBusinessLogic.calculateAppraisal(employee);
		assertEquals(500, appraisal,0.0);
		System.out.println(appraisal);
	}
	//test to check salary
	@Test
	public void TestCalculateYearlySalary() {
		employee.setName("Alex");
		employee.setAge(24);
		employee.setMonthlySalary(8000);
		double salary = empBusinessLogic.calculateYearlySalary(employee);
		assertEquals(96000, salary,0.0);
		System.out.println(salary);
	}

}
