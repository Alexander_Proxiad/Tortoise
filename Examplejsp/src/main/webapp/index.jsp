<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Examples of each JSTL tag</title>
</head>
<body>
	<p>CATCH TAG</p>
	<c:catch var="errorMsg">
		<% int x = 50/0; %>
	</c:catch>
	<p>${errorMsg}</p>
	
	<p>SET & OUT TAGS</p>
	<c:set var="victor" value="${7}"/>
	Victors age is <c:out value="${victor}" default="unknown" />
	
	<p>REMOVE TAG</p>
	<c:set var="name" value="Jon Snow" scope="session"/>
	<p> <c:out value="${name}"/></p>
	
	<c:remove var="name"/>
	<p><c:out value="${name}" /></p>
	
	<p>IF TAG</p>
	<c:set var="speed" value="63"/>
	<c:if test="${speed <= 55 }">
		<c:out value="Thanks for driving safely"/>
	</c:if>
	<c:if test="${speed > 55 }">
		<c:out value="You are driving a bmw"/>
	</c:if>
	
	<
	<c:set var="victor" value="${7 }"/>
	<c:set var="milena" value="${16 }"/>
	<c:set var="nicolas" value="${18 }"/>
	<c:choose>
		<c:when test="${victor>nicolas }">
			<c:out value="Victor is older than nicolas"/>
		</c:when>
		<c:when test="${victor>milena}">
			<c:out value="Victor is older than milena"/>
		</c:when>
		<c:otherwise>
		<c:out value="Victor is the youngest"/>
		</c:otherwise>
	</c:choose>
	
	<p>FOR EACH TAG</p>
	<c:forEach var="i" begin="1" end="5" step="1">
		<c:out value="${i}"/>
		<br/>
	</c:forEach>
	
	<p>FOR TOKENS</p>
	<c:forTokens items="red,white,blue" delims="," var="color">
		<c:out value="${color }"/><br/>
	</c:forTokens>
	
	<p>URL TAG</p>
	<c:url var="url" value="/hello.jsp" context="/JSTL_Demo" scope="session"/>
	<a href="${url }">${url }</a>
	
	<p>REDIRECT TAG</p>
	
	<p>IMPORT TAG</p>
	<c:import url="importmepls.jsp"/>
 	
	
	
</body>
</html>