package luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import luv2code.hibernate.demo.entity.Course;
import luv2code.hibernate.demo.entity.Instructor;
import luv2code.hibernate.demo.entity.InstructorDetail;
import luv2code.hibernate.demo.entity.Review;

public class DeleteCourseAndReviewsDemo2 {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.addAnnotatedClass(Review.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();
		
		try {
			
			session.beginTransaction();
			
			int theId = 12;
			Course tempCourse = session.get(Course.class, theId);
			
			System.out.println("The course is: " + tempCourse);
			
			System.out.println("The courses reviews are: " + tempCourse.getReviews());
			
			session.getTransaction().commit();
			System.out.println("DONE");
			
		}finally {
			session.close();
			factory.close();
		}

	}

}
