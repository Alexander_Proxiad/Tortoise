package luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import luv2code.hibernate.demo.entity.Course;
import luv2code.hibernate.demo.entity.Instructor;
import luv2code.hibernate.demo.entity.InstructorDetail;

public class getInstructorCoursesDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();
		
		try {
			
			session.beginTransaction();
			
			// get the instructor from the db
			int theId = 1;
			Instructor tempInstructor =  session.get(Instructor.class, theId);
			
			System.out.println("Instructor: " + tempInstructor);
			System.out.println("Courses : " + tempInstructor.getCourse());
			
			session.getTransaction().commit();
			System.out.println("Your create operation has executed successfully!!");
			
		}finally {
			session.close();
			factory.close();
		}

	}

}
