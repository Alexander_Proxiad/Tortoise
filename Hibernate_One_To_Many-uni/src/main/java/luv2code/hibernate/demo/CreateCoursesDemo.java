package luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import luv2code.hibernate.demo.entity.Course;
import luv2code.hibernate.demo.entity.Instructor;
import luv2code.hibernate.demo.entity.InstructorDetail;

public class CreateCoursesDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();
		
		try {
			
			session.beginTransaction();
			
			// get the instructor from the db
			int theId = 1;
			Instructor tempInstructor =  session.get(Instructor.class, theId);
			//create some courses
			Course tempCourse1 = new Course("Air Guitar - the Ultimate Guide");
			Course tempCourse2 = new Course("The fuss course");
			//add courses to instrucot
			tempInstructor.add(tempCourse1);
			tempInstructor.add(tempCourse2);
			//save the courses
			session.save(tempCourse1);
			session.save(tempCourse2);
			
			session.getTransaction().commit();
			System.out.println("Your create operation has executed successfully!!");
			
		}finally {
			session.close();
			factory.close();
		}

	}

}
