package luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import luv2code.hibernate.demo.entity.Course;
import luv2code.hibernate.demo.entity.Instructor;
import luv2code.hibernate.demo.entity.InstructorDetail;

public class EagerLazyDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();
		
		try {
			
			session.beginTransaction();
			
			// get the instructor from the db
			int theId = 1;
			Instructor tempInstructor =  session.get(Instructor.class, theId);
			
			System.out.println("MEEE Instructor: " + tempInstructor);
			
			System.out.println("MEEE Courses : " + tempInstructor.getCourse());
						
			session.getTransaction().commit();
			session.close();
			System.out.println("\nThe session is now closed\n");
			//close session before the lazy executes
			System.out.println("MEEE Courses : " + tempInstructor.getCourse());

			System.out.println("MEEE Your create operation has executed successfully!!");
			
		}finally {
//			session.close();
			factory.close();
		}

	}

}
