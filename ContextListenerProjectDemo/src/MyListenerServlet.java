

import javax.servlet.ServletContext;
import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Lifecycle Listener implementation class MyListenerServlet
 *
 */
@WebListener
public class MyListenerServlet implements ServletContextListener, ServletContextAttributeListener {

    /**
     * Default constructor. 
     */
    public MyListenerServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextAttributeListener#attributeAdded(ServletContextAttributeEvent)
     */
    public void attributeAdded(ServletContextAttributeEvent scae)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see ServletContextAttributeListener#attributeRemoved(ServletContextAttributeEvent)
     */
    public void attributeRemoved(ServletContextAttributeEvent scae)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent sce)  { 
    	System.out.println("\n##########################\n");
    	System.out.println("contextDestroyed method has been called in " + this.getClass().getName());
    	ServletContext servletContext = sce.getServletContext();
    	System.out.println(servletContext + " is destroyed");
    	System.out.println("\n##########################\n");
    }

	/**
     * @see ServletContextAttributeListener#attributeReplaced(ServletContextAttributeEvent)
     */
    public void attributeReplaced(ServletContextAttributeEvent scae)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent sce)  { 
    	System.out.println("\n##########################\n");
    	System.out.println("contextInitialized method has been called in " + this.getClass().getName());
    	ServletContext servletContext = sce.getServletContext();
    	System.out.println(servletContext + " is initialized or created");
    	System.out.println("\n##########################\n");
    }
	
}
