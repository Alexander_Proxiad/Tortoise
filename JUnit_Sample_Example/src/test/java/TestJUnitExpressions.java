import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class TestJUnitExpressions extends TestCase{
	
	protected double fValue1;
	protected double fValue2;

	@Before
	public void setUp() throws Exception {
		fValue1 = 2.0;
		fValue2 = 3.0;
	}

	@Test
	public void testAdd() {
		//count the number of test cases
		System.out.println("Number of test cases= " + this.countTestCases());
		
		//test getName
		String name = this.getName();
		System.out.println("The name of the test case is = " + name);
		
		//test setName
		this.setName("Sparta");
		String newName = this.getName();
		System.out.println("The new name of the test case is = " + newName);
	}
	
	public void tearDown() {
		
	}

}
