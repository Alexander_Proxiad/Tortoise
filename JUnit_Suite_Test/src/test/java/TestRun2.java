import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import junit.suite.MessageUtil;

public class TestRun2 {

	String message = "Robert";
	MessageUtil messageUtil = new MessageUtil(message);
//	@Ignore
	@Test
	public void testSalutationMessage() {
		System.out.println("Inside testSalutationMessage()");
		message = "Hi " + "Robert";
//		assertEquals(message, messageUtil.salutationMessage());
		assertThat(message).isEqualTo(messageUtil.salutationMessage());
	}
}
