import static org.junit.Assert.*;

import org.junit.Test;

import junit.suite.MessageUtil;

public class TestRun1 {

	String message = "Robert";
	MessageUtil messageUtil = new MessageUtil(message);
	
	@Test(expected = ArithmeticException.class)
	public void testPrintMessage() {
		System.out.println("Inside the testPrintMessage()");
		messageUtil.printMessage();
	}

}
