package spring.exercise;

public class Person {

	private int age;
	private String name;
	private Address address;

	public Person() {
		
	}
	
	

	public Person(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}



	public Person(int age, String name, Address address) {
		super();
		this.age = age;
		this.name = name;
		this.address = address;
	}

	public void displayInfo() {
		System.out.println(age + " " + name);
		System.out.println(address);
	}
}
