package spring.tutorial;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Question {

	private int id;
	private String name;
	Map<String, String> answersMap;

	public Question() {

	}

	public Question(int id, String name, Map<String, String> answersMap) {
		super();
		this.id = id;
		this.name = name;
		this.answersMap = answersMap;
	}

	public void displayInfo() {
		System.out.println(id + " " + name);
		System.out.println("answers:");
		Set<Entry<String, String>> set = answersMap.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Entry<String, String> entry = iterator.next();
			System.out.println("Answer: " + entry.getKey() + " Posted by " + entry.getValue());
		}

	}

}
