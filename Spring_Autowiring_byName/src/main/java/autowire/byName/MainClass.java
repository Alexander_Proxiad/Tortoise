package autowire.byName;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

@SuppressWarnings("deprecation")
public class MainClass {

	public static void main(String[] args) {
		BeanFactory factory = new XmlBeanFactory(new ClassPathResource("applicationContext.xml"));
		TextEditor te = (TextEditor)factory.getBean("textEditor");
		te.spellCheck();

	}

}
