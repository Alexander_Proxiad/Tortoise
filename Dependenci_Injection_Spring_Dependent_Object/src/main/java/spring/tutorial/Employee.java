package spring.tutorial;

public class Employee {

	private String name;
	private int id;
	private Address address;

	public Employee() {
		System.out.println("default constructor");
	}

	public Employee(String name, int id, Address address) {
		super();
		this.name = name;
		this.id = id;
		this.address = address;
	}

	public void show() {
		System.out.println(id + " " + name);
		System.out.println(address.toString());
	}

}
