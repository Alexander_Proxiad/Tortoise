package spring.crud.demo.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import spring.crud.entity.User;

@Service
public class UserServiceImpl implements UserService {

	private Map<Integer, User> userMap = new HashMap<>();// if its not initialized the value is null and u will throw
															// and exception

	private Integer userId = 0;// start from 0 so the first person will have an index of 1

	@Override
	public User selectById(int userId) {
		User user = userMap.get(userId);
		return user;
	}

	public void insertUser(String name, String password, String email, String gender, String country) {
		userId++;
		User user = new User();
		populateUser(name, password, email, gender, country, user, userId);
		userMap.put(userId, user);

	}

	public void updateUser(int userId, String name, String password, String email, String gender, String country) {
		User user = userMap.get(userId);
		populateUser(name, password, email, gender, country, user, userId);
		userMap.put(userId, user);
	}

	public void deleteUser(int userId) {
		userMap.remove(userId);
	}

	public Collection<User> listAll() {
		return userMap.values();
	}

	private void populateUser(String name, String password, String email, String gender, String country, User user,
			int userId) {
		user.setName(name);
		user.setPassword(password);
		user.setEmail(email);
		user.setGender(gender);
		user.setCountry(country);
		user.setUserId(userId);
	}

}