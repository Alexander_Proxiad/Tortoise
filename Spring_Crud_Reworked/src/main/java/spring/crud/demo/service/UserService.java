package spring.crud.demo.service;

import java.util.Collection;
import java.util.Map;

import spring.crud.entity.User;

public interface UserService {
	
	public User selectById(int userId);
	
	public void insertUser(String name, String password, String email, String gender, String country);
	
	public void updateUser(int userId, String name, String password, String email, String gender, String country);
	
	public void deleteUser(int userId);
	
	public Collection<User> listAll();

}
