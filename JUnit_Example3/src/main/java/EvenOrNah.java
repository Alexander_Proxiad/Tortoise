
public class EvenOrNah {
	
	private int number;

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	public boolean isEvenNumber(int number) {
		boolean result = false;
		if(number % 2 == 0) {
			result = true;
		}
		return result;
	}

}
