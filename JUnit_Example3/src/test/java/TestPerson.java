import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class TestPerson {
	
	String name = "Alex";
	Person person = new Person(name);

	@Test
	public void test() {
		assertThat(name).isEqualTo(person.display());
	}

}
