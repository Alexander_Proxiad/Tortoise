package spring.collection;

import java.util.Iterator;
import java.util.List;

public class Question {

	private int id;
	private String name;
	private List<Answer> answerList;

	public Question() {

	}

	public Question(int id, String name, List<Answer> answerList) {
		super();
		this.id = id;
		this.name = name;
		this.answerList = answerList;
	}

	public void displayInfo() {
		System.out.println(id + " " + name);
		System.out.println("And the answers are:");
		Iterator<Answer> iterator = answerList.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}

}
