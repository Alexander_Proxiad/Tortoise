package exercise.stock;

public interface StockService {
	
	public double getPrice(Stock stock);

}
