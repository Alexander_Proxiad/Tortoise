package spring.dependency;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
	
//	<bean id="person" class="spring.dependency.Person">
//	<constructor-arg value="Alex" type="String"></constructor-arg>
//	<constructor-arg value="Vulchev" type="String"></constructor-arg>
//	<constructor-arg value="24" type="int"></constructor-arg>
//	<constructor-arg value="500.0" type="double"></constructor-arg>
//</bean>
	
	@Bean
	public Person getPerson() {
		Person person = new Person("Alex","Vulchev",24,500.0);
		return person;
	}

}
