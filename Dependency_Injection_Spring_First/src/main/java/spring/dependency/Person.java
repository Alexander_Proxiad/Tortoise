package spring.dependency;

public class Person {
	private String firstName;
	private String lastName;
	private int age;
	private double salary;

	public Person() {

	}

	public Person(String firstName) {

		this.firstName = firstName;
	}

	public Person(String firstName, String lastName) {

		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Person(String firstName, String lastName, int age) {

		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}

	public Person(String firstName, String lastName, int age, double salary) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.salary = salary;
	}

	public void displayPersonInfo() {
		System.out.println(firstName + " " + lastName + " " + age + " " + salary);
	}

}
