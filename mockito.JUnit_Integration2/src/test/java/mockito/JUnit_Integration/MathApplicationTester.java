package mockito.JUnit_Integration;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import junit.framework.Assert;
@SuppressWarnings("deprecation")
//@RunWith(MockitoJUnitRunner.class)
public class MathApplicationTester {
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@InjectMocks
	MathApplication mathApp = new MathApplication();
	
	@Mock 
	CalculatorService calcService;

	@Test
	public void test() {
		 when(calcService.add(10.0,20.0)).thenReturn(30.00);
		Assert.assertEquals(mathApp.add(10.0, 20.0), 30.0,0);
//		verify(calcService).add(10.0,20.0);
	}

}
