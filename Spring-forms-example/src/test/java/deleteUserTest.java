import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import spring.crud.demo.service.UserServiceImpl;
import spring.crud.entity.User;

public class deleteUserTest {
	
	User user;
	UserServiceImpl usiObject;
	
	@Before
	public void initParams() {
		user = new User();
		usiObject = new UserServiceImpl();
	}

	@Test
	public void testDelete() {
		
		usiObject.insertUser("Sonia", "sonia123", "sonia@ger.de", "Female", "Germany");
		assertThat(usiObject.getUserMap()).isNotNull();
		usiObject.deleteUser(usiObject.getUserMap().get(1).getUserId());
		assertThat(usiObject.getUserMap()).isEmpty();
	}
	

}
