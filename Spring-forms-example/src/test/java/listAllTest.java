import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import spring.crud.demo.service.UserServiceImpl;
import spring.crud.entity.User;

public class listAllTest {
	
	User user;
	UserServiceImpl usiObject;
	
	@Before
	public void initParam() {
		user = new User();
		usiObject = new UserServiceImpl();
		
		usiObject.insertUser("Alex", "alex123","Alex@mail.bg", "Male", "Bulgaria");
		usiObject.insertUser("Maria", "maria123","Maria@mail.bg", "Female", "Bulgaria");
	}

	@Test
	public void test() {
		
		assertThat(usiObject.listAll()).hasSize(2);
		assertThat(usiObject).hasNoNullFieldsOrProperties();

	}

}
