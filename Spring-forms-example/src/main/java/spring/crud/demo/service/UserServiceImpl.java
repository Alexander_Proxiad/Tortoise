package spring.crud.demo.service;

import java.util.Map;

public class UserServiceImpl implements UserService {

	private Map<Integer, User> userMap;

	private Integer userId = 1;

	public User selectById(int userId) {
		// TODO Auto-generated method stub
		return null;
	}

	public void insertUser(String name, String password, String email, String gender, String country) {
		userId++;
		User user = new User();
		populateUser(name, password, email, gender, country, user);
		userMap.put(userId, user);

	}

	private void populateUser(String name, String password, String email, String gender, String country, User user) {
		user.setName(name);
		user.setPassword(password);
		user.setEmail(email);
		user.setGender(gender);
		user.setCountry(country);
	}

	public void updateUser(int userId, String name, String password, String email, String gender, String country) {
		User user = userMap.get(userId);
		populateUser(name, password, email, gender, country, user);
		userMap.put(userId, user);
	}

	public void deleteUser(int userId) {
		userMap.remove(userId);

	}

	public Map<Integer, User> userMap() {
		userMap.values();
		return null;
	}

}
