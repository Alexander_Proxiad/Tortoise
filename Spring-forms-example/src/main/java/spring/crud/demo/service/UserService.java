package spring.crud.demo.service;

import java.util.Map;

public interface UserService {
	
	public User selectById(int userId);
	
	public void insertUser(String name, String password, String email, String gender, String country);
	
	public void updateUser(int userId, String name, String password, String email, String gender, String countr);
	
	public void deleteUser(int userId);
	
	public Map<Integer,User> userMap();

}
