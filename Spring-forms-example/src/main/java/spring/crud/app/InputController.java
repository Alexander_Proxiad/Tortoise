package spring.crud.app;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import spring.crud.demo.service.User;

@Controller
public class InputController {
	
	static int userId;
	Map<Integer, User> userList = new HashMap<Integer, User>();
		
	@RequestMapping("/showForm")
	public String showInputForm() {
		return "adduserform";
	}
	
	@RequestMapping("/saveForm")
	public String saveForm(HttpServletRequest request,Model model) {
		userId++;
		String userName = request.getParameter("userName");
		String userPassword = request.getParameter("userPassword");
		String userEmail = request.getParameter("userEmail");
		String userGender = request.getParameter("userGender");
		String userCountry = request.getParameter("userCountry");
		
		User user = new User();
		user.setId(userId);
		user.setName(userName);
		user.setPassword(userPassword);
		user.setEmail(userEmail);
		user.setGender(userGender);
		user.setCountry(userCountry);
		
		userList.put(userId, user);
		model.addAttribute("userList", userList);
		
		return "adduser-success";
	}
	
	@RequestMapping("/showAllUsers")
	public String showUsers() {
		return "displayusers";
	}
	
	

}
