package mockito.JUnit_Integration;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import junit.framework.Assert;
@SuppressWarnings("deprecation")
@RunWith(MockitoJUnitRunner.class)
public class MathApplicationTester {
	
	@InjectMocks
	MathApplication mathApp = new MathApplication();
	
	@Mock 
	CalculatorService calcService;

	@Test
	public void test() {
		when(calcService.add(10.0,20.0)).thenReturn(30.00);
		Assert.assertEquals(mathApp.add(10.0, 20.0), 30.0,0);
	}

}
