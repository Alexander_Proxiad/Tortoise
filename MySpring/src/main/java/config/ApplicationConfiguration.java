package config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import spring.impl.Address;
import spring.impl.Person;

@Configuration
@ComponentScan("spring.impl")
public class ApplicationConfiguration {

	@Bean(name="person", initMethod="init")
	public Person beanPerson(Address dependency) {
//		Person person = new Person("Alex",24,dependency);
//		return person;
		return new Person("Alex", 24, dependency);
		
	}
	
	@Bean(name="address",destroyMethod="destroy")
	public Address beanAddress() {
		Address address = new Address("Plovdiv","Bulgaria");
		return address;
		
	}
	
}
