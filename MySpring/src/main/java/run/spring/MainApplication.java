package run.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import config.ApplicationConfiguration;
import spring.impl.Person;

public class MainApplication {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext anoContext = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
		Person person = anoContext.getBean("person",Person.class);
		person.display();
		

	}

}
