package spring.impl;

import org.springframework.stereotype.Component;

@Component
public class Address {

	private String city, country;

	public Address() {
		super();
	}
	
	

	public Address(String city, String country) {
		super();
		this.city = city;
		this.country = country;
		System.out.println("ADDRESS CONSTRUCTOR");
	}



	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Address [city=" + city + ", country=" + country + "]";
	}
	
	public void destroy() {
		System.out.println("Destruction!!!");
	}
	

}
