

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MyServlet2
 */

public class MyServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//set Content type to html
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		//get httpSession
		HttpSession session = request.getSession();
		// typecast or use toString method to the object
		String myName = session.getAttribute("uName").toString();
		String myPass = (String)session.getAttribute("uPass");
		out.println("Name: " + myName + " Password " + myPass);
		out.close();
	}

}
