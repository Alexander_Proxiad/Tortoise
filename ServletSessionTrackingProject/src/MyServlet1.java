

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MyServlet1
 */

public class MyServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//set content type and output source
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		//extract data from html file
		String name = request.getParameter("userName");
		String password= request.getParameter("userPassword");
		//printout the user info
		out.print("Hello " + name);
		//get HttpSession object
		HttpSession session = request.getSession();
		//save attributes in session
		session.setAttribute("uName", name);
		session.setAttribute("uPass", password);
		//connect to the second Servlet via link(hint u have content type html)!
		out.print("<br/>");
		out.print("<a href='welcome'>View Profile Details</a>");
		out.close();
		
		
	}

}
