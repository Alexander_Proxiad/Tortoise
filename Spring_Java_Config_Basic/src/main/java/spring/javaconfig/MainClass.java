package spring.javaconfig;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainClass {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		AnnotationConfigApplicationContext anotationContext = new AnnotationConfigApplicationContext(HelloWorldConfig.class);
		HelloWorld hw = anotationContext.getBean(HelloWorld.class);
		hw.setMessage("SPARTA");
		hw.getMessage();

	}

}
