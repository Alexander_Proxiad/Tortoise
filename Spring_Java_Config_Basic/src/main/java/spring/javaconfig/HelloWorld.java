package spring.javaconfig;

public class HelloWorld {
	
	private String message;

	public void getMessage() {
		System.out.println("This is : " + message);
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
