<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Example of Exeption language</title>
</head>
<body>

<p> what is 2 + 3 : ${2+3}</p>
<p>Is 2 > 3 : ${2 > 3}</p>
<p> what is 10/0: ${10/0 }</p>
<p>2C = ${(9.0 / 5.0) * 2.0 + 32.0 } F </p>
<p>6 is an: ${6 % 2 == 0 ?"even": "not even"}</p>

</body>
</html>