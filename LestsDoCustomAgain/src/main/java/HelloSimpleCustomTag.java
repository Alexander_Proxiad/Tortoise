import java.io.IOException;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.jasper.el.JspELException;

public class HelloSimpleCustomTag extends SimpleTagSupport {
	public void doTag() throws JspELException,IOException {
		JspWriter out = getJspContext().getOut();
		out.print("Hello Custom tag");
	}
}
