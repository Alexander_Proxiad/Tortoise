<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Call the parameters now!!!</title>
</head>
<body>
<jsp:include page="param.jsp">
	<jsp:param value="Alex" name="firstname"/>
	<jsp:param value="Kamenov" name="middlename"/>
	<jsp:param value="Vulchev" name="lastname"/>
	</jsp:include>
</body>
</html>