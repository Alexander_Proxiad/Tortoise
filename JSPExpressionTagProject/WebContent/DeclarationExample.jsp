<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Declaration Example</title>
</head>
<body>
<p>
	<%! 
	public double salaryBonus(double salary){
		return salary*0.10;
	}
	%>
</p>
<p>Bonus to a emp with salary of 1000 will be: <%= salaryBonus(1000) %></p>
</body>
</html>