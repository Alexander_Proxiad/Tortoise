<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Expression Excercise</title>
</head>
<body>
<p>Converting a string to upper case: <%= new String("pls let me remember this").toUpperCase() %></p>
<p>Summing two numbers for example 5:10 : <%= 5*10 %></p>
<p>is 5 less than 10 : <%= 5 < 10 %></p>
</body>
</html>