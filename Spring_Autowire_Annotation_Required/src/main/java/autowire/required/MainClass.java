package autowire.required;

import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

@SuppressWarnings("deprecation")
public class MainClass {

	public static void main(String[] args) {
		XmlBeanFactory factory = new XmlBeanFactory(new ClassPathResource("applicationContext.xml"));
		Student student = (Student)factory.getBean("Student");
		
		System.out.println("Name: " + student.getName());
		System.out.println("Age : " + student.getAge() );

	}

}
