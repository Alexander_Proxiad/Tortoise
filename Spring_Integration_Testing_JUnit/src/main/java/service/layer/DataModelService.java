package service.layer;

public interface DataModelService {
	
	boolean isValid(String input);

}
