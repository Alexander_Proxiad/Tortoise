import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import applicationConfig.AppConfig;
import service.layer.DataModelService;
import service.layer.FlaseClass;
import service.layer.MachineLearningService;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {AppConfig.class,FlaseClass.class})
public class MachineLearningTest {
	
	@Autowired
	@Qualifier("flaseClass")
	DataModelService dml;

	@Test
	public void test_dml_always_return_true() {
		assertThat(dml, instanceOf(FlaseClass.class));
		assertThat((dml.isValid("")),is(false));
	}

}
