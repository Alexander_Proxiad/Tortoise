package servletP;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Employee;
import model.Person;
import model.Address;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/start")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Person person = new Employee();
		person.setName("Alex");
		
		request.setAttribute("person", person);
		
		Employee emp = new Employee();
		Address add = new Address();
		add.setAddress("Bulgaria");
		emp.setAddress(add);
		emp.setId(1);
		emp.setName("Alex Vulchev");
		
		HttpSession session = request.getSession();
		session.setAttribute("employee", emp);
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher("home.jsp");
		rd.forward(request, response);
		
	}

}
