package exercise.office.springboothibernate.service;

import java.util.List;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import exercise.office.springboothibernate.entities.Employee;

@Repository

public class EmployeeDAOImpl extends GenericDaoImpl<Employee> implements EmployeeDAO {

	public void createEmployee(Employee employee) {
		super.save(employee);
	}

	@Override
	public List<Employee> listBy(String name) {
		return getSession().createCriteria(Employee.class).add(Restrictions.ilike("name", name)).list();
	}

	@Override
	public List<EmployeeBean> listAllEmployees() {

		return (List<EmployeeBean>) getSession().createCriteria(Employee.class).createAlias("location", "location")
				.setProjection(Projections.projectionList().add(Projections.property("name"), "name")
						.add(Projections.property("doj"), "doj").add(Projections.property("authority"), "authority")
						.add(Projections.property("salary"), "salary")
						.add(Projections.property("location.name"), "locationName"))
				.setResultTransformer(Transformers.aliasToBean(EmployeeBean.class)).list();

	}

}
