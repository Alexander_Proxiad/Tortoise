package exercise.office.springboothibernate.service;

import exercise.office.springboothibernate.entities.Location;

public interface LocationDao {

	Location getBy(String locationName);

}
