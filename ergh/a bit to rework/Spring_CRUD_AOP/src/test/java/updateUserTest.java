import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import spring.crud.entity.User;
import spring.crud.service.UserServiceImpl;

public class updateUserTest {
	
	UserServiceImpl usiObject;
	User user;
	
	@Before
	public void initParams() {
		usiObject = new UserServiceImpl();
		user = new User();
	}

	@Test
	public void test() {
		
		usiObject.insertUser("Alex", "123456a", "email.com", "Male", "Bulgaria");
		assertThat(usiObject.getUserMap()).isNotEmpty();
		usiObject.updateUser(1, "Kamen", "123456", "kamen@mail.bg", "Male", "Romania");
		assertEquals("Kamen", usiObject.getUserMap().get(1).getName());
		
	}

}
