import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import spring.crud.entity.User;
import spring.crud.service.UserServiceImpl;

public class InsertUserTest {
	
	UserServiceImpl usiObject;
	User user;

	@BeforeClass
	   public static void beforeClass() {
	      System.out.println("Starting Test on UserServiceImpl");
	   }
	
	
	@Before
	public void init() {
		usiObject = new UserServiceImpl();
		user = new User();
	}
	
	
	
	@Test
	public void test() {
		
		usiObject.insertUser("Alex", "alex123","Alex@mail.bg", "Male", "Bulgaria");
		usiObject.insertUser("Maria", "maria123","Maria@mail.bg", "Female", "Bulgaria");
		
		assertThat(usiObject.getUserMap()).isNotEmpty().hasSize(2);
		assertThat(usiObject.getUserMap()).doesNotContainKey(3);
		
		assertEquals("Alex", usiObject.getUserMap().get(1).getName());
		assertEquals("alex123", usiObject.getUserMap().get(1).getPassword());
		assertEquals("Alex@mail.bg",usiObject.getUserMap().get(1).getEmail());
		assertEquals("Male", usiObject.getUserMap().get(1).getGender());
		assertEquals("Bulgaria",usiObject.getUserMap().get(1).getCountry());
		assertEquals("Female", usiObject.getUserMap().get(2).getGender());
		
	}
	
	@AfterClass
	public static void afterClass() {
		System.out.println("-------------------");
		System.out.println("All the test are finished");
	}

}
