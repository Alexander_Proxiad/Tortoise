package exercise.office.springboothibernate.service;

import java.util.List;

import exercise.office.springboothibernate.entities.Employee;

public interface EmployeeDAO extends GenericDao<Employee> {

	public void createEmployee(Employee employee);
	
	List<Employee> listBy(String firstName);
	
}