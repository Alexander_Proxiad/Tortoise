package exercise.office.springboothibernate.service;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import exercise.office.springboothibernate.entities.Location;

@Repository
public class LocationDaoImpl extends GenericDaoImpl<Location> implements LocationDao {

	@Override
	public Location getBy(String locationName) {

		return (Location) getSession().createCriteria(Location.class).add(Restrictions.eq("location", locationName))
				.uniqueResult();

	}

}
