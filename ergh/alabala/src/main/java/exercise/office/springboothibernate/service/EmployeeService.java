package exercise.office.springboothibernate.service;

import java.util.Collection;

import exercise.office.springboothibernate.entities.Employee;

public interface EmployeeService {
	
	Collection<Employee> filter();
	
	void update(EmployeeBean employeeBean);

}
