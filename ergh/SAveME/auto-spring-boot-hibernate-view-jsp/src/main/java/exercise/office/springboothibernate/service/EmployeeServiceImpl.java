package exercise.office.springboothibernate.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import exercise.office.springboothibernate.entities.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private LocationDao locationDao;
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Override
	public Collection<Employee> filter() {
		Collection<Employee> filteredList = new ArrayList<Employee>();
		Collection<Employee> list = employeeDAO.getAll();
		for (Employee employee : list) {
			if (employee.getName().startsWith("B")) {
				filteredList.add(employee);
			}
		}
		// TODO Auto-generated method stub
		return filteredList;
	}
	
	@Override
	public void update(EmployeeBean employeeBean) {
		Employee employee = employeeDAO.get(employeeBean.getId());
		if(employee == null) {
			employee = new Employee();
		}
		populate(employee, employeeBean);
		employeeDAO.update(employee);
		
	}

	private void populate(Employee employee, EmployeeBean employeeBean) {
		employee.setName(employeeBean.getName());
		employee.setAuthority(employeeBean.getAuthority());
		employee.setDoj(employeeBean.getDoj());
		employee.setSalary(employeeBean.getSalary());
		employee.setLocation(locationDao.getBy(employeeBean.getLocationName()));
	}

	@Override
	public List<EmployeeBean> getAllEmployees() {
		
		return employeeDAO.listAllEmployees();
	}

	@Override
	public void deleteEmployee(int employeeId) {
		
		employeeDAO.deleteById(employeeId);
	}

	
	
}
