package exercise.office.springboothibernate.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AddSeleniumTest {

	public static void main(String[] args) {
		try {
			WebDriver driver = new ChromeDriver();
			driver.get("http://localhost:8080/view");
			WebElement addButton = driver.findElement(By.xpath("/html/body/input"));
			addButton.click();
			WebElement nameField = driver.findElement(By.xpath("//*[@id=\"name\"]"));
			nameField.sendKeys("Dimitar");
			WebElement dateField = driver.findElement(By.xpath("//*[@id=\"doj\"]"));
			dateField.sendKeys("12/12/12 12:12:12");
			WebElement dropMenuAuthority = driver.findElement(By.xpath("//*[@id=\"authority\"]"));
			Select selectAuthority = new Select(dropMenuAuthority);
			selectAuthority.selectByVisibleText("Admin");
			WebElement dropMenuLocation = driver.findElement(By.xpath("//*[@id=\"locationName\"]"));
			Select selectLocation = new Select(dropMenuLocation);
			selectLocation.selectByVisibleText("Sofia");
			WebElement salaryField = driver.findElement(By.xpath("//*[@id=\"salary\"]"));
			salaryField.sendKeys("1250.0");
			Thread.sleep(5000);
			WebElement saveButton = driver.findElement(By.xpath("//*[@id=\"employee\"]/table/tbody/tr[6]/td[2]/input"));
			saveButton.click();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
