package office.exercise;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.everyItem;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.mockito.internal.matchers.GreaterThan;

public class AssertJMatcherTest {

	@Test
	public void test() {
		List<Integer> scores = Arrays.asList(99,100,101,105);
		assertThat(scores).hasSize(4);
		assertThat(scores).contains(99,105);
		
		assertThat("").isEmpty();
		Integer[] mars	 = {1,2,3,4};
		assertThat(mars).hasSize(4);
		assertThat(mars).contains(3,2,4,1);
	}

}
