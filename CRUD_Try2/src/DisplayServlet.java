

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DisplayServlet
 */

public class DisplayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");
		/*String userName = (String) session.getAttribute(user.getName());
		String userId = (String) session.getAttribute(user.getId());
		String userPassword = (String) session.getAttribute(user.getPassword());
		String userEmail= (String) session.getAttribute(user.getEmail());
		
		out.print("UserID: " + userId + "Username: " + userName + " UserPassword " + userPassword + " UserEmail " + userEmail);*/
		out.println("user details " + user + " ");
		out.print("<a href='delete'>Delete user</a>");
		out.close();
		
	}
}


