

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DisplayServlet
 */

public class SaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String userId = request.getParameter("userId");
		String userName = request.getParameter("userName");
		String userPassword = request.getParameter("userPassword");
		String userEmail = request.getParameter("userEmail");
		
		User user = new User();
		
		user.setId(userId);
		user.setName(userName);
		user.setPassword(userPassword);
		user.setEmail(userEmail);
		
		
		HttpSession session = request.getSession();
		
		
		session.setAttribute("user", user);
		response.sendRedirect("display");
		//out.print("<a href='display'>View Userlist</a><br/>");
		out.close();
		
	}

	

}
