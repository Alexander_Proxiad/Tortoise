package scane.me.pls;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class RandomFortuneService implements FortuneService {

	private String[] data = {
			"beware of the wolf in sheep's clothing",
			"the journey is the reward",
			"pidar blyat"
	};
	
	private Random myRandom = new Random();
	
	
	@Override
	public String getFortune() {

		int index = myRandom.nextInt(data.length);
		
		String theFortune = data[index];
		
		return theFortune;
	}
}
