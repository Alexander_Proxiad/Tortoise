package scane.me.pls;

public interface Coach {
	
	public String getDailyWorkout();
	
	public double pricePerLesson(double price);
	
	public String getDailyFortune();

}
