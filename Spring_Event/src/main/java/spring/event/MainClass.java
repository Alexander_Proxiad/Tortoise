package spring.event;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {

	public static void main(String[] args) {
		ConfigurableApplicationContext ccontext = new ClassPathXmlApplicationContext("applicationContext.xml");
		ccontext.start();
		HelloWorld obj = (HelloWorld)ccontext.getBean("HelloWorld");
		obj.getMessage();
		ccontext.stop();
	}

}
