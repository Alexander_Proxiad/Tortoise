package spring.collection;

public class Answer {

	private int id;
	private String name;
	private String by;

	public String getBy() {
		return by;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setBy(String by) {
		this.by = by;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Answer [id=" + id + ", name=" + name + ", by=" + by + "]";
	}

}
