package spring.collection;

import java.util.Iterator;
import java.util.List;

public class Question {

	private int id;
	private String name;
	private List<Answer> answerList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Answer> getAnswerList() {
		return answerList;
	}

	public void setAnswerList(List<Answer> answerList) {
		this.answerList = answerList;
	}

	public void printInfo() {
		System.out.println(id + " " + name);
		System.out.println("Asnwers: ");
		Iterator<Answer> iterator = answerList.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
}
