package exercise;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SaveUserServlet
 */
@WebServlet("/SaveUserServlet")
public class SaveUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String firstName= request.getParameter("firstName");
		String lastName= request.getParameter("lastName");
		String age = request.getParameter("age");
		int numberAge = Integer.parseInt(age);
		String country= request.getParameter("country");
		
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setAge(numberAge);
		user.setCountry(country);
		
		int status=UserDao.saveUser(user);
		if(status>0) {
			out.print("<p> Record save successfully</p>");
			request.getRequestDispatcher("index.html").include(request, response);
		}else {
			out.println("Sorry unable to save the record");
		}
		out.close();
	}

}
