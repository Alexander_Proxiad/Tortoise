package exercise;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
	
	public static Connection getConnection() {
		Connection con=null;
		try {
			Class.forName("org.h2.Driver");
			con = DriverManager.getConnection("jdbc:h2:C:\\DEV\\Tools\\Eclipse_Workspace\\CRUD_Users\\lib", "alex_crud", "123");
		}catch(Exception e) {
			e.printStackTrace();
		}
		return con;
	}
	
	public static int saveUser(User user) {
		int status=0;
		try {
			Connection con = UserDao.getConnection();
			 PreparedStatement ppst = con.prepareStatement("insert into users(firstname,lastname,age,country)values(?,?,?,?)");
			 ppst.setString(1, user.getFirstName());
			 ppst.setString(2, user.getLastName());
			 ppst.setInt(3, user.getAge());
			 ppst.setString(4, user.getCountry());
			 
			 status=ppst.executeUpdate();
			 con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public static int updateUser(User user) {
		int status=0;
		try {
			Connection con = UserDao.getConnection();
			PreparedStatement ppst = con.prepareStatement("update users set firstname=?,lastname=?,age=?,country=? where id=?");
			ppst.setString(1, user.getFirstName());
			ppst.setString(2, user.getLastName());
			ppst.setInt(3, user.getAge());
			ppst.setString(4, user.getCountry());
			ppst.setInt(5, user.getId());
			
			status = ppst.executeUpdate();
			con.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return status;
	}
	
	public static int deleteUser(int id) {
		int status = 0;
		try {
			Connection con=UserDao.getConnection();
			PreparedStatement ppst = con.prepareStatement("delete from users where id=?");
			ppst.setInt(1, id);
			status=ppst.executeUpdate();
			con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public static User getUser(int id) {
		User user = new User();
		try {
			Connection con = UserDao.getConnection();
			PreparedStatement ppst = con.prepareStatement("select * from users where id=?");
			ppst.setInt(1, id);
			ResultSet rs = ppst.executeQuery();
			if (rs.next()){
				user.setId(rs.getInt(1));
				user.setFirstName(rs.getString(2));
				user.setLastName(rs.getString(3));
				user.setAge(rs.getInt(4));
				user.setCountry(rs.getString(5));
			}
			con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return user;
	}
	
	public static List<User> getAllUsers(){
		List<User> list = new ArrayList<User>();
		try {
			Connection con = UserDao.getConnection();
			PreparedStatement ppst = con.prepareStatement("select * from users");
			ResultSet rs = ppst.executeQuery();
			while(rs.next()) {
				User user = new User();
				user.setId(rs.getInt(1));
				user.setFirstName(rs.getString(2));
				user.setLastName(rs.getString(3));
				user.setAge(rs.getInt(4));
				user.setCountry(rs.getString(5));
				list.add(user);
			}
			con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
		
	}

}
