package exercise;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ViewServlet
 */
@WebServlet("/ViewServlet")
public class ViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<a href='index.html'>Add new User</a>" );
		out.print("<h1>Users list</h1>");
		
		List<User>list=UserDao.getAllUsers();
		out.print("<table border='1' width='100%'");
		out.print("<tr><th>firstName</th><th>lastName</th><th>age</th><th>Email</th><th>country</th><th>Edit</th><th>Delete</th></tr>");
		for(User user:list){
			out.print("<tr><td>"+user.getId()+"<tr><td>"+user.getFirstName()+"</td><td>"+user.getLastName()+"</td><td>"+user.getAge()+"</td><td>"+user.getCountry()+"</td><td> <a href='EditServlet?id="+user.getId()+"'>edit</a></td><td><a href='DeleteServlet?firstName="+user.getId()+"'>delete</a></td></tr>");
		}
		out.print("</table>");
		
		out.close();
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

}
