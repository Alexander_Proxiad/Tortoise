package mockito_MyTry;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class DogImplementationTest {

	

	@Test
	public void test() {
		DogoService dogoServiceMock = mock(DogoService.class);
		List<String> dogs = Arrays.asList("Huski is the best","Shepards are smarter","But Huski is prettier" );
		when(dogoServiceMock.retrieveHuski("HuskiPLS")).thenReturn(dogs);
		DogImplementation di = new DogImplementation(dogoServiceMock);
		List<String> filteredDogs = di.retrieveDosThatAreHuskies("HuskiPLS");
		assertEquals(2, filteredDogs.size());
	}

}
