package mockito_MyTry;

import java.util.ArrayList;
import java.util.List;

public class DogImplementation {
	
	private DogoService dogoService;

	public DogImplementation(DogoService dogoService) {
		super();
		this.dogoService = dogoService;
	}
	
	public List<String> retrieveDosThatAreHuskies(String dog){
		
		List<String> filteredDog = new ArrayList<String>();
		List<String> dogs = dogoService.retrieveHuski(dog);
		
		for(String doge : dogs) {
			if(doge.contains("Huski")) {
				filteredDog.add(doge);
			}
		}
		return filteredDog;
		
	}

}
