import static org.junit.Assert.assertEquals;

import org.junit.Test;

import test.me.pls.MessageUtil;

public class TestJUnit {

//	@Test
//	public void test() {
//		fail("Not yet implemented");
//	}
	
	String message = "Hello World";
	MessageUtil messageUtil = new MessageUtil(message);
	
	@Test
	public void testPrintMessage() {
		assertEquals(message, messageUtil.printMessage());
	}

}
