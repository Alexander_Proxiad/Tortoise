package spring.tutorial;

//import org.springframework.beans.factory.BeanFactory;
//import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.core.io.ClassPathResource;

public class MainApp {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		HelloWorld objA = (HelloWorld)context.getBean("helloWorld");
		objA.getMessage1();
		objA.getMessage2();
		
		HelloBulgaria objB = (HelloBulgaria)context.getBean("helloBulgaria");
		objB.getMessage1();
		objB.getMessage2();
		objB.getMessage3();
		((ConfigurableApplicationContext)context).close();
		
//		ClassPathResource resource = new ClassPathResource("Beans.xml");
//		BeanFactory factory = new XmlBeanFactory(resource);
//		
//		HelloWorld objA = (HelloWorld)factory.getBean("helloWorld");
//		objA.getMessage1();
//		objA.getMessage2();
//		
//		HelloBulgaria objB = (HelloBulgaria)factory.getBean("helloBulgaria");
//		objB.getMessage1();
//		objB.getMessage2();
//		objB.getMessage3();
	}

}
