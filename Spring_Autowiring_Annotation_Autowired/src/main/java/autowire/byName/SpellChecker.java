package autowire.byName;

public class SpellChecker {
	
	public SpellChecker() {
		System.out.println("inside spellchecker constructor");
	}
	
	public void checkSpelling() {
		System.out.println("inside checkSpelling method");
	}

}
