package autowire.byName;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SuppressWarnings("deprecation")
public class MainClass {

	public static void main(String[] args) {
//		XmlBeanFactory factory = new XmlBeanFactory(new ClassPathResource("applicationContext.xml"));
//		TextEditor te = (TextEditor)factory.getBean("textEditor");
//		te.spellCheck();
		
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

	      TextEditor te = (TextEditor) context.getBean("TextEditor");

	      te.spellCheck();

	}

}
